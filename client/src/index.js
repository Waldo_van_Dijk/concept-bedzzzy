import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import firmhouseClient from './Apollo/client';
import { ApolloProvider } from '@apollo/client';
import { CREATE_CART } from './Apollo/mutations';

if (typeof window !== 'undefined') {
  let cartSubscriptionToken = localStorage.getItem(
    'firmhouseSubscriptionToken'
  );
  if (cartSubscriptionToken == null) {
    firmhouseClient
      .mutate({ mutation: CREATE_CART, variables: { input: {} } })
      .then((result) => {
        cartSubscriptionToken = result.data.createCart.cart.token;
        localStorage.setItem(
          'firmhouseSubscriptionToken',
          cartSubscriptionToken
        );
      });
  }
}

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={firmhouseClient}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
