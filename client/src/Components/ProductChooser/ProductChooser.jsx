import Product from '../Product/Product';
import './productchooser.styles.scss';

const ProductsChooser = ({ productData, addProducts }) => {
  const { loading, error, data } = productData;
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :( {error}</p>;
  return (
    <div className='productchooser'>
      {data.products.nodes.map((data) => {
        return (
          <Product key={data.id} product={data} addProducts={addProducts} />
        );
      })}
    </div>
  );
};
export default ProductsChooser;
