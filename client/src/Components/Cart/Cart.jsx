import { useEffect, useState } from 'react';
import CartItem from './Item/Item';
import './cart.styles.scss';
import PlanSelector from '../PlanSelector/PlanSelector';

const Cart = ({
  cartObject,
  deleteOrderedProduct,
  planData,
  updatePlans,
  updateQuantity,
}) => {
  const { data, refetch, error, loading } = cartObject;
  const [openCart, setOpenCart] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const checkOut = () => {
    window.open(data.currentCart.checkoutUrl, '_blank');
  };

  useEffect(() => {
    if (data) {
      if (data.currentCart) {
        if (
          data.currentCart.orderedProducts.length > 0 &&
          data.currentCart.activePlan
        ) {
          setDisableButton(false);
        } else {
          setDisableButton(true);
        }
      }
    }
  }, [data]);

  const removeProduct = (id) => {
    deleteOrderedProduct({
      variables: {
        input: {
          id: parseInt(id, 10),
        },
      },
    });
    refetch();
  };

  const updateProduct = (slug, quantity, amount) => {
    console.log(slug, quantity, amount);
    updateQuantity(slug, quantity, amount);
    refetch();
  };

  if (error) return <p>Error :( {error}</p>;
  if (loading) return <p>Loading...</p>;
  return (
    <>
      <div
        className='cart'
        onClick={() => {
          refetch();
          setOpenCart(!openCart);
        }}
      >
        Cart
      </div>
      <div>
        {openCart && (
          <div className='cart__dropdown'>
            <div className='cart__header'>
              <div className='cart__header--amount'>
                Items in cart: {data.currentCart.orderedProducts.length}
              </div>
              <div className='cart__header--price'>
                Total: &euro;
                {data.currentCart.initialAmountIncludingTaxCents / 100}
              </div>
            </div>
            <div className='cart__body'>
              {data.currentCart.orderedProducts.map((prod, key) => {
                const price =
                  (prod.quantity * prod.product.priceIncludingTaxCents) / 100;
                return (
                  <CartItem
                    key={key}
                    id={prod.id}
                    img={prod.img}
                    name={prod.name}
                    quantity={prod.quantity}
                    slug={prod.product.slug}
                    price={price}
                    removeProduct={removeProduct}
                    updateProduct={updateProduct}
                  />
                );
              })}
            </div>
            <div className='cart__selector'>
              <PlanSelector
                planData={planData}
                currentPlanSlug={data.currentCart.activePlan}
                updatePlans={updatePlans}
              />
            </div>
            <button
              onClick={checkOut}
              className='cart__footer'
              disabled={disableButton}
            >
              Bestel nu
            </button>
          </div>
        )}
      </div>
    </>
  );
};

export default Cart;
