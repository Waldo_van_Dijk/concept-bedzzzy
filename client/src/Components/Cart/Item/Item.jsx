const CartItem = (props) => {
  const {
    img,
    name,
    price,
    quantity,
    id,
    removeProduct,
    updateProduct,
    slug,
  } = props;
  console.log(slug);
  return (
    <div className='cart__item'>
      <img className='cart__item--img' src={img} alt='product img' />
      <div className='cart__item--details'>
        <div className='cart__item--name'>{name}</div>
        <div>
          <div className='cart__item--amount'>
            <button onClick={() => updateProduct(slug, quantity, -1)}>-</button>
            <div className='cart__item--quantity'>Quantity: {quantity}</div>
            <button onClick={() => updateProduct(slug, quantity, 1)}>+</button>
          </div>

          <button onClick={() => removeProduct(id)}>Remove</button>
        </div>

        <div className='cart__item--price'>&euro;{price}</div>
      </div>
    </div>
  );
};
export default CartItem;
