import { useEffect, useState } from 'react';

export default function PlanSelector({
  currentPlanSlug,
  planData,
  updatePlans,
}) {
  const { loading, data, error } = planData;
  const [slug, setSlug] = useState('');

  useEffect(() => {
    console.log(currentPlanSlug);
    if (currentPlanSlug) {
      setSlug(currentPlanSlug.slug);
    }
  }, [currentPlanSlug]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :( {error}</p>;

  return (
    <select
      name='plan'
      value={slug}
      onChange={(e) => {
        e.preventDefault();
        setSlug(e.target.value);
        console.log(e.target.value);
        console.log(currentPlanSlug);
        if (e.target.value !== null) {
          updatePlans({
            variables: { input: { planSlug: e.target.value } },
          });
        }
      }}
    >
      {!slug && <option value=''>Select one...</option>}
      {data.plans.nodes.map(({ id, slug, name }) => {
        return (
          <option key={id} value={slug}>
            Plan: {name}
          </option>
        );
      })}
    </select>
  );
}
