import './navbar.styles.scss';
import Cart from '../Cart/Cart';

const Navbar = ({
  cartObject,
  deleteOrderedProduct,
  planData,
  updatePlans,
  updateQuantity,
}) => {
  return (
    <div className='navbar'>
      <div className='navbar__logo'>
        <img
          className='navbar__logo--img'
          src='https://bedzzzy.com/wp-content/uploads/2019/07/bedzzzy-logo.svg'
          alt='logo'
        ></img>
      </div>
      <div className='navbar__links'>
        <div className='navbar__links--link'>Home</div>
        <div className='navbar__links--link'>Onze missie</div>
        <div className='navbar__links--link'>Zo werkt het</div>
        <div className='navbar__links--link'>Het matras</div>
        <div className='navbar__links--link'>Het bed</div>
        <div className='navbar__links--link'>FAQ</div>
        <div className='navbar__links--link'>Blog</div>
      </div>
      <div className='navbar__cart'>
        <Cart
          cartObject={cartObject}
          deleteOrderedProduct={deleteOrderedProduct}
          planData={planData}
          updatePlans={updatePlans}
          updateQuantity={updateQuantity}
        />
      </div>
    </div>
  );
};

export default Navbar;
