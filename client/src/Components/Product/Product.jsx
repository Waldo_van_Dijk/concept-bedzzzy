import { useState } from 'react';
import './product.styles.scss';

const Product = ({ product, addProducts }) => {
  const { title, slug, priceCents } = product;

  const [quantity, setQuantity] = useState(0);

  return (
    <div className='product'>
      <div className='product__title'>Product: {title}</div>
      <div className='product__price'>&euro; {priceCents},-</div>
      <div className='product__checkout'>
        <div className='product__quantity'>
          <button
            className='product__quantity--btn'
            onClick={() => {
              setQuantity((prevState) => prevState - 1);
            }}
            disabled={quantity <= 0}
          >
            -
          </button>
          <input
            type='number'
            className='product__quantity--number'
            value={quantity}
            disabled
          />
          <button
            className='product__quantity--btn'
            onClick={() => {
              setQuantity((prevState) => prevState + 1);
            }}
          >
            +
          </button>
        </div>
        <button
          disabled={quantity <= 0}
          className='product__button'
          onClick={() => addProducts(slug, quantity)}
        >
          Add to cart
        </button>
      </div>
    </div>
  );
};

export default Product;
