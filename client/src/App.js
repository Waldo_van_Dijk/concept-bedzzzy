import Navbar from './Components/Navbar/Navbar';

import ProductChooser from './Components/ProductChooser/ProductChooser';
import {
  DELETE_PRODUCT,
  UPDATE_CART,
  UPDATE_PLAN,
  UPDATE_QUANTITY,
} from './Apollo/mutations';
import { CURRENT_CART, PLANS, PRODUCTS } from './Apollo/query';
import { useQuery, useMutation } from '@apollo/client';

import './App.scss';

function App() {
  const planData = useQuery(PLANS);
  const [updatePlans] = useMutation(UPDATE_PLAN);

  const cart = useQuery(CURRENT_CART);
  const [createOrderedProduct] = useMutation(UPDATE_CART);
  const [updateOrderedProductQuantity] = useMutation(UPDATE_CART);

  const productData = useQuery(PRODUCTS);
  const [deleteOrderedProduct] = useMutation(DELETE_PRODUCT);

  const addProducts = (slug, quantity) => {
    if (quantity > 0) {
      createOrderedProduct({
        variables: {
          input: {
            orderedProduct: {
              slug: slug,
              quantity: parseInt(quantity, 10),
            },
          },
        },
      }).then((data) => console.log(data));
      cart.refetch();
    }
  };

  const updateQuantity = (slug, quantity, amount) => {
    if ((quantity > 1 && amount < 0) || (amount >= 0 && amount > 0)) {
      updateOrderedProductQuantity({
        variables: {
          input: {
            orderedProduct: {
              slug: slug,
              quantity: parseInt(amount, 10),
            },
          },
        },
      }).then((data) => console.log(data));
      cart.refetch();
    }
  };
  if (cart.error || planData.error || productData.error)
    return <p>Error :( {cart.rror}</p>;
  if (cart.loading) return <p>Loading...</p>;
  console.log(cart);
  return (
    <div className='main'>
      <Navbar
        cartObject={cart}
        deleteOrderedProduct={deleteOrderedProduct}
        planData={planData}
        updatePlans={updatePlans}
        updateQuantity={updateQuantity}
      />
      <div className='main__body'>
        <ProductChooser productData={productData} addProducts={addProducts} />
      </div>
    </div>
  );
}

export default App;
