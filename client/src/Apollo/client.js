import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: 'https://firmhouse-pa-storefront-hgmhpj.herokuapp.com/graphql',
});

const authLink = setContext((context, { headers }) => {
  return {
    headers: {
      ...headers,
      'X-Project-Access-Token': '8xPrTCNPWALFimNf58HnnNQw',
      'X-Subscription-Token':
        typeof window === 'undefined'
          ? null
          : localStorage.getItem('firmhouseSubscriptionToken'),
    },
  };
});

export default new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({
    typePolicies: {
      Subscription: {
        keyFields: ['token'],
      },
      Cart: {
        keyFields: ['token'],
      },
    },
  }),
});
