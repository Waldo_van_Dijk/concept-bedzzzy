import { gql } from '@apollo/client';

const CURRENT_CART = gql`
  query CurrentCart {
    currentCart {
      token
      initialAmountIncludingTaxCents
      checkoutUrl
      activePlan {
        id
        name
        slug
      }
      orderedProducts {
        id
        title
        quantity

        product {
          priceIncludingTaxCents
          slug
        }
      }
    }
  }
`;

const PLANS = gql`
  query GetPlans {
    plans {
      nodes {
        id
        slug
        name
      }
    }
  }
`;

const PRODUCTS = gql`
  query GetProducts {
    products {
      nodes {
        id
        title
        slug
        priceCents
      }
    }
  }
`;

export { CURRENT_CART, PLANS, PRODUCTS };
