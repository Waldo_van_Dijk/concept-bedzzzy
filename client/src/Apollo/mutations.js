import { gql } from '@apollo/client';

const CREATE_CART = gql`
  mutation CreateCart($input: CreateCartInput!) {
    createCart(input: $input) {
      cart {
        token
        initialAmountIncludingTaxCents
        activePlan {
          id
          name
          slug
        }
      }
    }
  }
`;

const DELETE_PRODUCT = gql`
  mutation DestroyOrderedProduct($input: DestroyOrderedProductInput!) {
    destroyOrderedProduct(input: $input) {
      subscription {
        token
        id
      }
    }
  }
`;

const UPDATE_CART = gql`
  mutation CreateOrderedProduct($input: CreateOrderedProductInput!) {
    createOrderedProduct(input: $input) {
      subscription {
        token
        orderedProducts {
          id
          title
          quantity
        }
      }
    }
  }
`;

const UPDATE_QUANTITY = gql`
  mutation UpdateOrderedProductQuantity(
    $input: UpdateOrderedProductQuantityInput!
  ) {
    updateOrderedProductQuantity(input: $input) {
      subscription {
        token
        orderedProducts {
          id
          title
          quantity
        }
      }
    }
  }
`;
const UPDATE_PLAN = gql`
  mutation UpdatePlan($input: UpdatePlanInput!) {
    updatePlan(input: $input) {
      cart {
        token
        initialAmountIncludingTaxCents
        activePlan {
          id
          name
          slug
        }
      }
    }
  }
`;

export {
  CREATE_CART,
  DELETE_PRODUCT,
  UPDATE_CART,
  UPDATE_PLAN,
  UPDATE_QUANTITY,
};
